package com.losevskiyf.spring_boot.dao;

import com.losevskiyf.spring_boot.entity.Fine;
import com.losevskiyf.spring_boot.exceptions_handling.NoSuchFineException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class Database {
    private final List<Fine> fines = new ArrayList<>();

    public List<Fine> getAllFines(){
        return fines;
    }

    public Fine getFine(int id){
        for(Fine f : fines){
            if(f.getId()==id) return f;
        }
        throw new NoSuchFineException("Can't get. Fine with that ID="+id+" doesn't exist");
    }

    public void saveFine(Fine fine){
        if(!fines.contains(fine)){
            fines.add(fine);
            return;
        }
        throw new NoSuchFineException("Can't add. Fine with that ID="+fine.getId()+" already exists");
    }
    public void updateFine(Fine fine){
        if(fines.contains(fine)){
            fines.remove(fine);
            fines.add(fine);
            return;
        }
        throw new NoSuchFineException("Can't update. Fine with that ID="+fine.getId()+" doesn't exist");
    }

    public void deleteFine(int id){
        for(Fine f : fines){
            if(f.getId()==id){
                fines.remove(f);
                return;
            }
        }
        throw new NoSuchFineException("Can't delete. Fine with that ID="+id+" doesn't exist");
    }

    public void payFine(int id) {
        Fine result;
        for(Fine f : fines){
            if(f.getId()==id) {
                result = f;
                fines.remove(f);
                result.setPaid(true);
                fines.add(result);
                return;
            }
        }
        throw new NoSuchFineException("Can't pay. Fine with that ID="+id+" doesn't exist");
    }

    public void sendSubpoena(int id){
        Fine result;
        for(Fine f : fines){
            if(f.getId()==id) {
                result = f;
                fines.remove(f);
                result.setHasSubpoena(true);
                fines.add(result);
                return;
            }
        }
        throw new NoSuchFineException("Can't send. Fine with that ID="+id+" doesn't exist");
    }

}