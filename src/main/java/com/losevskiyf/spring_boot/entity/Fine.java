package com.losevskiyf.spring_boot.entity;

import com.fasterxml.jackson.annotation.JsonTypeId;

import java.time.LocalDateTime;

public class Fine {
    private static int count = 0;
    private int id = ++count;
    private String carNumber;
    private String violatorName;
    private String inspectorName;
    private LocalDateTime timeOfTheProtocol;
    private int amount;
    private boolean hasSubpoena;
    private boolean isPaid;
    private LocalDateTime paymentDate;
    private LocalDateTime paymentDeadline;

    public Fine(
            String carNumber,
            String violatorName,
            String inspectorName,
            LocalDateTime timeOfTheProtocol,
            int amount, boolean hasSubpoena,
            boolean isPaid,
            LocalDateTime paymentDate,
            LocalDateTime paymentDeadline
    ) {
        this.carNumber = carNumber;
        this.violatorName = violatorName;
        this.inspectorName = inspectorName;
        this.timeOfTheProtocol = timeOfTheProtocol;
        this.amount = amount;
        this.hasSubpoena = hasSubpoena;
        this.isPaid = isPaid;
        this.paymentDate = paymentDate;
        this.paymentDeadline = paymentDeadline;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getViolatorName() {
        return violatorName;
    }

    public void setViolatorName(String violatorName) {
        this.violatorName = violatorName;
    }

    public String getInspectorName() {
        return inspectorName;
    }

    public void setInspectorName(String inspectorName) {
        this.inspectorName = inspectorName;
    }

    public LocalDateTime getTimeOfTheProtocol() {
        return timeOfTheProtocol;
    }

    public void setTimeOfTheProtocol(LocalDateTime timeOfTheProtocol) {
        this.timeOfTheProtocol = timeOfTheProtocol;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isHasSubpoena() {
        return hasSubpoena;
    }

    public void setHasSubpoena(boolean hasSubpoena) {
        this.hasSubpoena = hasSubpoena;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public LocalDateTime getPaymentDeadline() {
        return paymentDeadline;
    }

    public void setPaymentDeadline(LocalDateTime paymentDeadline) {
        this.paymentDeadline = paymentDeadline;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Fine)
            return ((Fine) obj).getId() == getId();
        else
            return false;
    }

}
