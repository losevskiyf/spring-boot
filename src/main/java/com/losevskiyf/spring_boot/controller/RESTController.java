package com.losevskiyf.spring_boot.controller;

import com.losevskiyf.spring_boot.dao.Database;
import com.losevskiyf.spring_boot.entity.Fine;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RESTController {

    private final Database database;

    public RESTController(Database database) {
        this.database = database;
    }

    @GetMapping("/fines")
    public List<Fine> showAllEmployees(){
        return database.getAllFines();
    }

    @GetMapping("/fines/{id}")
    public Fine getFine(@PathVariable int id){
        return database.getFine(id);
    }

    @PostMapping("/fines")
    public Fine addNewFine(@RequestBody Fine fine){
        database.saveFine(fine);
        return fine;
    }

    @PutMapping("/fines")
    public Fine putFine(@RequestBody Fine fine){
        database.updateFine(fine);
        return fine;
    }

    @DeleteMapping("/fines/{id}")
    public String deleteFine(@PathVariable int id){
        database.deleteFine(id);
        return "Fine with ID = " + id + " was deleted";
    }

    @PatchMapping("/fines/{id}/pay")
    public String payFine(@PathVariable int id){
        database.payFine(id);
        return "Fine with ID = " + id + " was paid";
    }

    @PatchMapping("/fines/{id}/court")
    public String sendSubpoena(@PathVariable int id){
        database.sendSubpoena(id);
        return "Fine with ID = " + id + " was sent in court";
    }

}
