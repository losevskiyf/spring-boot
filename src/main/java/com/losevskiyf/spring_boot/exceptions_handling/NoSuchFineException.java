package com.losevskiyf.spring_boot.exceptions_handling;

public class NoSuchFineException extends RuntimeException {
    public NoSuchFineException(String message) {
        super(message);
    }
}