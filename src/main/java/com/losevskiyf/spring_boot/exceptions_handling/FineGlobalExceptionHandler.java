package com.losevskiyf.spring_boot.exceptions_handling;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class FineGlobalExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<FineIncorrectData> handleException(NoSuchFineException exception){
        FineIncorrectData data = new FineIncorrectData();
        data.setInfo(exception.getMessage());
        return new ResponseEntity<>(data, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<FineIncorrectData> handleException(Exception exception){
        FineIncorrectData data = new FineIncorrectData();
        data.setInfo(exception.getMessage());
        return new ResponseEntity<>(data, HttpStatus.BAD_REQUEST);
    }

}