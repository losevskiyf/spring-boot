package com.losevskiyf.spring_boot.exceptions_handling;

public class FineIncorrectData{
    public FineIncorrectData() {
    }

    private String info;

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }
}