CREATE DATABASE  fines;
USE fines;

CREATE TABLE `Protocol` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`car_id` INT NOT NULL,
	`violator_id` INT NOT NULL,
	`inspector_id` INT NOT NULL,
	`fine_id` INT NOT NULL,
	`time` TIMESTAMP NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Violator` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`first_name` varchar(255) NOT NULL,
	`second_name` varchar(255) NOT NULL,
	`last_name` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Car` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`number` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Violator'sCar` (
	`violator_id` INT NOT NULL,
	`car_id` INT NOT NULL,
	PRIMARY KEY (`violator_id`,`car_id`)
);

CREATE TABLE `Inspector` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`first_name` varchar(255) NOT NULL,
	`second_name` varchar(255) NOT NULL,
	`last_name` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Fine` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`has_subpoena` BOOLEAN NOT NULL,
	`is_paid` BOOLEAN NOT NULL,
	`payment_date` TIMESTAMP NOT NULL,
	`payment_deadline` TIMESTAMP NOT NULL,
	`amount` INT NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `Protocol` ADD CONSTRAINT `Protocol_fk0` FOREIGN KEY (`car_id`) REFERENCES `Car`(`id`);

ALTER TABLE `Protocol` ADD CONSTRAINT `Protocol_fk1` FOREIGN KEY (`violator_id`) REFERENCES `Violator`(`id`);

ALTER TABLE `Protocol` ADD CONSTRAINT `Protocol_fk2` FOREIGN KEY (`inspector_id`) REFERENCES `Inspector`(`id`);

ALTER TABLE `Protocol` ADD CONSTRAINT `Protocol_fk3` FOREIGN KEY (`fine_id`) REFERENCES `Fine`(`id`);

ALTER TABLE `Violator'sCar` ADD CONSTRAINT `Violator'sCar_fk0` FOREIGN KEY (`violator_id`) REFERENCES `Violator`(`id`);

ALTER TABLE `Violator'sCar` ADD CONSTRAINT `Violator'sCar_fk1` FOREIGN KEY (`car_id`) REFERENCES `Car`(`id`);

